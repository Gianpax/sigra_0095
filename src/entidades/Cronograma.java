package entidades;

import java.sql.Date;

public class Cronograma 
{
	private int idCronograma;
	private String nombreActividad;
	private String descripcionActiviadad;
	private Date inicioActividad;
	private Date finActividad;
	private int idFase;
	private int idEstado;
	private int estado;
	
	public int getIdCronograma() {
		return idCronograma;
	}
	public void setIdCronograma(int idCronograma) {
		this.idCronograma = idCronograma;
	}
	public String getNombreActividad() {
		return nombreActividad;
	}
	public void setNombreActividad(String nombreActividad) {
		this.nombreActividad = nombreActividad;
	}
	public String getDescripcionActiviadad() {
		return descripcionActiviadad;
	}
	public void setDescripcionActiviadad(String descripcionActiviadad) {
		this.descripcionActiviadad = descripcionActiviadad;
	}
	public Date getInicioActividad() {
		return inicioActividad;
	}
	public void setInicioActividad(Date inicioActividad) {
		this.inicioActividad = inicioActividad;
	}
	public Date getFinActividad() {
		return finActividad;
	}
	public void setFinActividad(Date finActividad) {
		this.finActividad = finActividad;
	}
	public int getIdFase() {
		return idFase;
	}
	public void setIdFase(int idFase) {
		this.idFase = idFase;
	}
	public int getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
}
