package entidades;

import java.time.LocalDate;

public class Tema {
	int idTema;
	LocalDate fechaCreacion;
	String nombreTema;
	String descripcion;
	int idAmbito;
	String nombreArchivo;
	int idCronogramaEsp;
	
	public int getIdCronogramaEsp() {
		return idCronogramaEsp;
	}
	public void setIdCronogramaEsp(int idCronogramaEsp) {
		this.idCronogramaEsp = idCronogramaEsp;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public int getIdTema() {
		return idTema;
	}
	public void setIdTema(int idTema) {
		this.idTema = idTema;
	}
	public LocalDate getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(LocalDate fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getNombre() {
		return nombreTema;
	}
	public void setNombre(String nombre) {
		this.nombreTema = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getIdAmbito() {
		return idAmbito;
	}
	public void setIdAmbito(int idAmbito) {
		this.idAmbito = idAmbito;
	}
}
