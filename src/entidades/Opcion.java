package entidades;

public class Opcion {
	int idOpcionl;
	String descOpcion;
	
	public int getIdOpcionl() {
		return idOpcionl;
	}
	public void setIdOpcionl(int idOpcionl) {
		this.idOpcionl = idOpcionl;
	}
	public String getDescOpcion() {
		return descOpcion;
	}
	public void setDescOpcion(String descOpcion) {
		this.descOpcion = descOpcion;
	}
	
}
