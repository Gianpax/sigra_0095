package entidades;

public class DetalleSeminario {
	int idDetalleSeminario;
	int idGrupoFCE;
	int idSeminario;
	
	public int getIdDetalleSeminario() {
		return idDetalleSeminario;
	}
	public void setIdDetalleSeminario(int idDetalleSeminario) {
		this.idDetalleSeminario = idDetalleSeminario;
	}
	public int getIdGrupoFCE() {
		return idGrupoFCE;
	}
	public void setIdGrupoFCE(int idGrupoFCE) {
		this.idGrupoFCE = idGrupoFCE;
	}
	public int getIdSeminario() {
		return idSeminario;
	}
	public void setIdSeminario(int idSeminario) {
		this.idSeminario = idSeminario;
	}
	
}
