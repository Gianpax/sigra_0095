package entidades;

public class EstadoActividad 
{
	private int idEstadoAct;
	private String estadoAct;
	public int getIdEstadoAct() {
		return idEstadoAct;
	}
	public void setIdEstadoAct(int idEstadoAct) {
		this.idEstadoAct = idEstadoAct;
	}
	public String getEstadoAct() {
		return estadoAct;
	}
	public void setEstadoAct(String estadoAct) {
		this.estadoAct = estadoAct;
	}
}
