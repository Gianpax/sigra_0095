package entidades;

public class PalabraClave {
	int idPalabraClave;
	String palabraclave;
	
	
	public int getIdPalabraClave() {
		return idPalabraClave;
	}
	public void setIdPalabraClave(int idPalabraClave) {
		this.idPalabraClave = idPalabraClave;
	}
	public String getPalabraclave() {
		return palabraclave;
	}
	public void setPalabraclave(String palabraclave) {
		this.palabraclave = palabraclave;
	}
	
	
}
