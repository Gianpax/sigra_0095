package entidades;

public class GrupoFCE {
	int idGrupoFCE;
	int idEstudiante;
	int idTema;
	
	public int getIdGrupoFCE() {
		return idGrupoFCE;
	}
	public void setIdGrupoFCE(int idGrupoFCE) {
		this.idGrupoFCE = idGrupoFCE;
	}
	public int getIdEstudiante() {
		return idEstudiante;
	}
	public void setIdEstudiante(int idEstudiante) {
		this.idEstudiante = idEstudiante;
	}
	public int getIdTema() {
		return idTema;
	}
	public void setIdTema(int idTema) {
		this.idTema = idTema;
	}
	
}
