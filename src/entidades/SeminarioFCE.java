package entidades;

public class SeminarioFCE {
	int idSeminario;
	String numGrupo;
	int idTutor;
	
	public int getIdSeminario() {
		return idSeminario;
	}
	public void setIdSeminario(int idSeminario) {
		this.idSeminario = idSeminario;
	}
	public String getNumGrupo() {
		return numGrupo;
	}
	public void setNumGrupo(String numGrupo) {
		this.numGrupo = numGrupo;
	}
	public int getIdTutor() {
		return idTutor;
	}
	public void setIdTutor(int idTutor) {
		this.idTutor = idTutor;
	}
	
	
}
