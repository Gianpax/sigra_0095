package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.SeminarioFCE;


public class DtSeminario {
	PoolConexion poolConexion = PoolConexion.getInstance();
	Connection connection = PoolConexion.getConnection();
	ResultSet resultSet = null;
	
	public ArrayList<SeminarioFCE> listarSeminarios()
	{
		ArrayList<SeminarioFCE> seminario = new ArrayList<SeminarioFCE>();
		String sql = "SELECT * FROM public.\"tblSeminarioFCE\"";
		
		try 
		{
			PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			resultSet = ps.executeQuery();
			
			while(resultSet.next())
			{
				SeminarioFCE sem= new SeminarioFCE();
				sem.setIdSeminario(resultSet.getInt("idSeminario"));
				sem.setNumGrupo(resultSet.getString("numGrupo"));
				sem.setIdTutor(resultSet.getInt("idTutor"));
				seminario.add(sem);
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("DATOS: ERROR AL OBTENER Seminarios");
			e.printStackTrace();
		}
		return seminario;
	}
	
	public boolean guardarSeminario(SeminarioFCE sem)
	{
		boolean guardado = false;
		try 
		{
			this.listarSeminarios();
			resultSet.moveToInsertRow();
			resultSet.updateString("numGrupo", sem.getNumGrupo());
			resultSet.updateInt("idTutor", sem.getIdTutor());
			
			resultSet.insertRow();
			resultSet.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR -> Error al guardar Seminario " + e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarSeminario(SeminarioFCE sem)
	{
		
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = "DELETE FROM public.\"tblSeminarioFCE\" WHERE \"idSeminario\" = ?";
		
		try 
		{
			ps = connection.prepareStatement(sql);
			ps.setInt(1, sem.getIdSeminario());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR -> Error al eliminar Seminario " + e.getMessage());
			e.printStackTrace();
		}
	
		return eliminado;
	}
	
	public boolean modificarSeminario(SeminarioFCE sem)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = "UPDATE public.\"tblSeminarioFCE\" " + 
				"	SET \"numGrupo\" = ?, \"idTutor\" = ?" + 
				"	WHERE \"idSeminario\" = ?";
				
		try 
		{
 			ps = connection.prepareStatement(sql);
			ps.setString(1, sem.getNumGrupo());
			ps.setInt(2, sem.getIdTutor());
			ps.setInt(3, sem.getIdSeminario());
			ps.executeUpdate();
			modificado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR-> Error al modificar seminario" +e.getMessage());
			e.printStackTrace();
		}
		return modificado;
	}
	
}
