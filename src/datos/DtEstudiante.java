package datos;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import entidades.Estudiante;


public class DtEstudiante {
	
	PoolConexion poolConexion = PoolConexion.getInstance();
	Connection connection = PoolConexion.getConnection();
	ResultSet resultSet = null;
	
	public ArrayList<Estudiante> listarEstudiante()
	{
		ArrayList<Estudiante> est = new ArrayList<Estudiante>();
		String sql = "SELECT * from public.\"tblEstudiantes\" ";
		
		try 
		{
			PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			resultSet = ps.executeQuery();
			
			while(resultSet.next())
			{
				Estudiante estu= new Estudiante();
				estu.setIdEstudiante(resultSet.getInt("idEstudiante"));
				estu.setIdUsuario(resultSet.getInt("idUsuario"));
				est.add(estu);
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("DATOS: ERROR AL OBTENER ESTUDIANTES");
			e.printStackTrace();
		}
		return est;
	}
	
	public boolean guardarEstudiante(int idUsuario)
	{
		boolean guardado = false;
		try 
		{
			this.listarEstudiante();
			resultSet.moveToInsertRow();
			resultSet.updateInt("idUsuario", idUsuario);
			
			resultSet.insertRow();
			resultSet.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR -> Error al guardar Estudiante " + e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
}
