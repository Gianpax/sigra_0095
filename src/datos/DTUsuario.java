package datos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.User;
import vistas.Usuario_vs_Rol_Opciones;

public class DTUsuario {

	PoolConexion poolConexion = PoolConexion.getInstance();
	Connection connection = PoolConexion.getConnection();
	ResultSet resultSet = null;
	
	public ArrayList<User> listarUsuario()
	{
		ArrayList<User> user = new ArrayList<User>();
		String sql = "SELECT * from public.\"tblUsuarios\" ";
		
		try 
		{
			PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			resultSet = ps.executeQuery();
			
			while(resultSet.next())
			{
				User u= new User();
				u.setIdUsuario(resultSet.getInt("idUsuario"));
				u.setUsuario(resultSet.getString("usuario"));
				u.setPassword(resultSet.getString("password"));
				u.setEstado(resultSet.getInt("estado"));
				u.setNombres(resultSet.getString("nombres"));
				u.setApellidos(resultSet.getString("apellidos"));
				u.setSexo(resultSet.getBoolean("sexo"));
				u.setFechaNac(resultSet.getDate("fechaNac").toLocalDate());
				u.setCorreo(resultSet.getString("correo"));
				u.setCarnet(resultSet.getString("carnet"));
				u.setTelefono(resultSet.getString("telefono"));
				user.add(u);
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("DATOS: ERROR AL OBTENER USUARIOS");
			e.printStackTrace();
		}
		return user;
	}
	
	public ArrayList<User> listarUsuariosActivo()
	{
		ArrayList<User> user = new ArrayList<User>();
		String sql = "SELECT * from public.\"tblUsuarios\" where estado != 3 ";
		
		try 
		{
			PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			resultSet = ps.executeQuery();
			
			while(resultSet.next())
			{
				User u= new User();
				u.setIdUsuario(resultSet.getInt("idUsuario"));
				u.setUsuario(resultSet.getString("usuario"));
				u.setPassword(resultSet.getString("password"));
				u.setEstado(resultSet.getInt("estado"));
				u.setNombres(resultSet.getString("nombres"));
				u.setApellidos(resultSet.getString("apellidos"));
				u.setSexo(resultSet.getBoolean("sexo"));
				u.setFechaNac(resultSet.getDate("fechaNac").toLocalDate());
				u.setCorreo(resultSet.getString("correo"));
				u.setCarnet(resultSet.getString("carnet"));
				u.setTelefono(resultSet.getString("telefono"));
				user.add(u);
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("DATOS: ERROR AL OBTENER USUARIOS");
			e.printStackTrace();
		}
		return user;
	}
	
	
	public boolean guardarUsuario(User u)
	{
		boolean guardado = false;
		try 
		{
			this.listarUsuario();
			resultSet.moveToInsertRow();
			resultSet.updateInt("idRol", u.getIdRol());
			resultSet.updateString("usuario", u.getUsuario());
			resultSet.updateString("password", u.getPassword());
			resultSet.updateString("nombres", u.getNombres());
			resultSet.updateString("apellidos", u.getApellidos());
			resultSet.updateBoolean("sexo", u.getSexo());
			resultSet.updateString("correo", u.getCorreo());
			resultSet.updateString("carnet", u.getCarnet());
			resultSet.updateString("telefono", u.getTelefono());
			resultSet.updateDate("fechaNac", Date.valueOf(u.getFechaNac()));
			
			resultSet.insertRow();
			resultSet.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR -> Error al guardar Usuario " + e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean LoginUsuario(Usuario_vs_Rol_Opciones u)
	{
		boolean encontrado = false;
		
		PreparedStatement ps;
		String sql = ("SELECT * FROM public.usuario_vs_roles_opciones WHERE usuario = ? AND password = ? AND \"idRol\" = ? AND estado != 3");
		try 
		{
			ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ps.setString(1, u.getUsuario());
			ps.setString(2, u.getPassword());
			ps.setInt(3, u.getIdRol());
			
			resultSet = ps.executeQuery();
			
			if(resultSet.next())
			{
				encontrado = true;
			}
			else
			{
				encontrado = false;
			}
			
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL VERIFICAR EL LOGIN "+ e.getMessage());
			e.printStackTrace();
		}
		
		return encontrado;
	}

	public boolean eliminarUsuario(User u)
	{
		/*
		 * Estados
		 * 1 - Agregado
		 * 2 - Modificado
		 * 3 - Eliminado
		 *  */
		
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = "Update public.\"tblUsuarios\" set estado=3 where \"idUsuario\" = ?";
		
		try 
		{
			ps = connection.prepareStatement(sql);
			ps.setInt(1, u.getIdUsuario());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR -> Error al eliminar usuario " + e.getMessage());
			e.printStackTrace();
		}
	
		return eliminado;
	}
	
	public boolean modificarUsuario(User u)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = "UPDATE public.\"tblUsuarios\"\r\n" + 
				"	SET usuario=?, password=?, nombres=?, apellidos=?, sexo=?, correo=?, carnet=?, telefono=?, estado=2, \"fechaNac\"=?\r\n" + 
				"	WHERE \"idUsuario\" = ?";
				
		try 
		{
 			ps = connection.prepareStatement(sql);
			ps.setString(1, u.getUsuario());
			ps.setString(2, u.getPassword());
			ps.setString(3, u.getNombres());
			ps.setString(4, u.getApellidos());
			ps.setBoolean(5, u.getSexo());
			ps.setString(6, u.getCorreo());
			ps.setString(7, u.getCarnet());
			ps.setString(8, u.getTelefono());
			ps.setDate(9, Date.valueOf(u.getFechaNac()));
			ps.setInt(10, u.getIdUsuario());
			ps.executeUpdate();
			modificado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR-> Error al modificar usuario" +e.getMessage());
			e.printStackTrace();
		}
		return modificado;
	}

	public int getIdUsuario(User u){
		int idUsuario = 0;
		try {
			PreparedStatement ps = connection.prepareStatement("Select * From public.\"tblUsuarios\" where usuario = ?", ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			ps.setString(1, u.getUsuario());
			resultSet = ps.executeQuery();
			
			while(resultSet.next())
			{
				idUsuario = resultSet.getInt("idUsuario");
			}
			
		} catch (Exception e) {
			System.out.println("error al obtener idUsuario: "+e.getMessage());
			return idUsuario;
		}
		return idUsuario;
	}
	
	public ArrayList<User> listarEstudiantes()
	{
		ArrayList<User> user = new ArrayList<User>();
		String sql = "Select * from public.\"tblUsuarios\" as u " + 
				"inner join public.\"tblUserRol\" as ur on u.\"idUsuario\" = ur.\"idUsuario\" " + 
				"inner join public.\"tblRol\" as r on ur.\"idRol\" = r.\"idRol\" " + 
				"where ur.\"idRol\" = 2 and estado != 3";
		
		try 
		{
			PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			resultSet = ps.executeQuery();
			
			while(resultSet.next())
			{
				User u= new User();
				u.setIdUsuario(resultSet.getInt("idUsuario"));
				u.setUsuario(resultSet.getString("usuario"));
				u.setPassword(resultSet.getString("password"));
				u.setEstado(resultSet.getInt("estado"));
				u.setNombres(resultSet.getString("nombres"));
				u.setApellidos(resultSet.getString("apellidos"));
				u.setSexo(resultSet.getBoolean("sexo"));
				u.setFechaNac(resultSet.getDate("fechaNac").toLocalDate());
				u.setCorreo(resultSet.getString("correo"));
				u.setCarnet(resultSet.getString("carnet"));
				u.setTelefono(resultSet.getString("telefono"));
				user.add(u);
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("DATOS: ERROR AL OBTENER USUARIOS");
			e.printStackTrace();
		}
		return user;
	}
	
	public ArrayList<User> listarTutores()
	{
		ArrayList<User> user = new ArrayList<User>();
		String sql = "Select * from public.\"tblUsuarios\" as u " + 
				"inner join public.\"tblUserRol\" as ur on u.\"idUsuario\" = ur.\"idUsuario\" " + 
				"inner join public.\"tblRol\" as r on ur.\"idRol\" = r.\"idRol\" " + 
				"where ur.\"idRol\" =  3 and estado != 3";
		
		try 
		{
			PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			resultSet = ps.executeQuery();
			
			while(resultSet.next())
			{
				User u= new User();
				u.setIdUsuario(resultSet.getInt("idUsuario"));
				u.setUsuario(resultSet.getString("usuario"));
				u.setPassword(resultSet.getString("password"));
				u.setEstado(resultSet.getInt("estado"));
				u.setNombres(resultSet.getString("nombres"));
				u.setApellidos(resultSet.getString("apellidos"));
				u.setSexo(resultSet.getBoolean("sexo"));
				u.setFechaNac(resultSet.getDate("fechaNac").toLocalDate());
				u.setCorreo(resultSet.getString("correo"));
				u.setCarnet(resultSet.getString("carnet"));
				u.setTelefono(resultSet.getString("telefono"));
				user.add(u);
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("DATOS: ERROR AL OBTENER USUARIOS");
			e.printStackTrace();
		}
		return user;
	}
	
}
