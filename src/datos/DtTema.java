package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.SeminarioFCE;
import entidades.Tema;

public class DtTema {
	PoolConexion poolConexion = PoolConexion.getInstance();
	Connection connection = PoolConexion.getConnection();
	ResultSet resultSet = null;
	
	public ArrayList<Tema> listarTemas()
	{
		ArrayList<Tema> temas = new ArrayList<Tema>();
		String sql = "SELECT * FROM public.\"tblTemas\"";
		
		try 
		{
			PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			resultSet = ps.executeQuery();
			
			while(resultSet.next())
			{
				Tema tema = new Tema();
				tema.setIdTema(resultSet.getInt("idTema"));
				tema.setNombre(resultSet.getString("nombreTema"));
				tema.setFechaCreacion(resultSet.getDate("fechaCreacion").toLocalDate());
				tema.setIdAmbito(resultSet.getInt("idAmbito"));
				tema.setIdCronogramaEsp(resultSet.getInt("idCronogramaEsp"));
				tema.setNombreArchivo(resultSet.getString("nombreArchivo"));
				tema.setDescripcion(resultSet.getString("descripcion"));
				temas.add(tema);
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("DATOS: ERROR AL OBTENER TEMAS");
			e.printStackTrace();
		}
		return temas;
	}
}
