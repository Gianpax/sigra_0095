package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import entidades.Cronograma;

public class DTCronograma 
{
	PoolConexion pc = PoolConexion.getInstance();
	Connection cn = PoolConexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<Cronograma> listarCronograma()
	{
		ArrayList<Cronograma> cronogramas = new ArrayList<Cronograma>();
		String sql = "SELECT * FROM public.cronograma where \"idEstadoActividad\" <> 3 and \"estado\" <> 3;";
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Cronograma c = new Cronograma();
				c.setIdCronograma(rs.getInt("idCronograma"));
				c.setNombreActividad(rs.getString("nombreActividad"));
				c.setDescripcionActiviadad(rs.getString("descripcion"));
				c.setInicioActividad(rs.getDate("fechaInicio"));
				c.setFinActividad(rs.getDate("fechaFin"));
				c.setIdFase(rs.getInt("idFase"));
				c.setIdEstado(rs.getInt("idEstadoActividad"));
				c.setEstado(rs.getInt("estado"));
				cronogramas.add(c);
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("DATOS: ERROR AL OBTENER EL CRONOGRAMA");
			e.printStackTrace();
		}
		return cronogramas;
	}
	
	public ArrayList<Cronograma> listarCronogramas(int idF)
	{
		ArrayList<Cronograma> cronogramas = new ArrayList<Cronograma>();
		String sql = "SELECT * FROM public.cronograma where \"idFase\" = ? and \"idEstadoActividad\" <> 3 and \"estado\" <> 3;";
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			ps.setInt(1, idF);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Cronograma c = new Cronograma();
				c.setIdCronograma(rs.getInt("idCronograma"));
				c.setNombreActividad(rs.getString("nombreActividad"));
				c.setDescripcionActiviadad(rs.getString("descripcion"));
				c.setInicioActividad(rs.getDate("fechaInicio"));
				c.setFinActividad(rs.getDate("fechaFin"));
				c.setIdFase(rs.getInt("idFase"));
				c.setIdEstado(rs.getInt("idEstadoActividad"));
				c.setEstado(rs.getInt("estado"));
				cronogramas.add(c);
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("DATOS: ERROR AL OBTENER EL CRONOGRAMA");
			e.printStackTrace();
		}
		return cronogramas;
	}
	
	public boolean guardarCronograma(Cronograma c)
	{
		boolean guardado = false;
		
		try 
		{
			this.listarCronograma();
			rs.moveToInsertRow();
			rs.updateString("nombreActividad", c.getNombreActividad());
			rs.updateString("descripcion", c.getDescripcionActiviadad());
			rs.updateDate("fechaInicio", c.getInicioActividad());
			rs.updateDate("fechaFin", c.getFinActividad());
			rs.updateInt("idFase", c.getIdFase());
			rs.updateInt("idEstadoActividad", c.getIdEstado());
			rs.updateInt("estado", 1);
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR -> Error al guardar Cronograma " + e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarCronograma(Cronograma c)
	{
		/*
		 * Estados
		 * 1 - Agregado
		 * 2 - Modificado
		 * 3 - Eliminado
		 *  */
		
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = "Update public.cronograma set estado = 3 where \"idCronograma\" = ?";
		
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, c.getIdCronograma());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR -> Error al Actividad " + e.getMessage());
			e.printStackTrace();
		}
		
		return eliminado;
	}
	
	public boolean modificarActividad(Cronograma c)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = "Update public.cronograma set \"nombreActividad\" = ?, \"descripcion\" = ?, \"fechaInicio\" = ?, \"fechaFin\" = ?, \"idFase\" = ?, \"idEstadoActividad\" = ?, estado = 2 where \"idCronograma\" = ?;";
				
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setString(1, c.getNombreActividad());
			ps.setString(2, c.getDescripcionActiviadad());
			ps.setDate(3, c.getInicioActividad());
			ps.setDate(4, c.getFinActividad());
			ps.setInt(5, c.getIdFase());
			ps.setInt(6, c.getIdEstado());
			ps.setInt(7, c.getIdCronograma());
			ps.executeUpdate();
			modificado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR-> Error al modificar Actividad " +e.getMessage());
			e.printStackTrace();
		}
		return modificado;
	}
}
