package datos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.Fase;

public class DTFase 
{
	PoolConexion pc = PoolConexion.getInstance();
	Connection cn = PoolConexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<Fase> listarFase()
	{
		ArrayList<Fase> Fases = new ArrayList<Fase>();
		String sql = "SELECT \"idFase\", \"nombreFase\", to_char(\"faseInicio\", 'yyyy-MM-dd') as \"faseInicio\", to_char(\"faseFin\", 'yyyy-MM-dd') as \"faseFin\" FROM public.fase;";
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Fase f = new Fase();
				f.setId(rs.getInt("idFase"));
				f.setTitle(rs.getString("nombreFase"));
				f.setStart(rs.getString("faseInicio"));
				f.setEnd(rs.getString("faseFin"));
				Fases.add(f);
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("DATOS: ERROR AL OBTENER LAS FASES");
			e.printStackTrace();
		}
		return Fases;
	}
	
	public boolean guardarFase(Fase f)
	{
		boolean guardado = false;
		
		try 
		{
			this.listarFase();
			rs.moveToInsertRow();
			rs.updateString("nombreFase", f.getTitle());
			rs.updateDate("faseInicio", Date.valueOf(f.getStart()));
			rs.updateDate("faseFin", Date.valueOf(f.getEnd()));
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR -> Error al guardar Fase " + e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean modificarFase(Fase f)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = "Update public.fase set \"faseInicio\" = ?, \"faseFin\" = ? where \"idFase\" = ?;";
				
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setDate(1, Date.valueOf(f.getStart()));
			ps.setDate(2, Date.valueOf(f.getEnd()));
			ps.setInt(3, f.getId());
			ps.executeUpdate();
			modificado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR-> Error al modificar Actividad " +e.getMessage());
			e.printStackTrace();
		}
		return modificado;
	}

}
