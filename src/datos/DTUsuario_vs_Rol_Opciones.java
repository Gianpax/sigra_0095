package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vistas.Usuario_vs_Rol_Opciones;

public class DTUsuario_vs_Rol_Opciones {
	PoolConexion pc = PoolConexion.getInstance();
	Connection cn = PoolConexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<Usuario_vs_Rol_Opciones> permisosUsuario(String usuario,int idRol)
	{
		ArrayList<Usuario_vs_Rol_Opciones> permisosUsuarios = new ArrayList<Usuario_vs_Rol_Opciones>();
		String sql = "SELECT * from public.usuario_vs_roles_opciones where usuario = ? AND \"idRol\" = ?";
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			ps.setString(1, usuario);
			ps.setInt(2, idRol);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Usuario_vs_Rol_Opciones uro = new Usuario_vs_Rol_Opciones();
				uro.setIdUsuario(rs.getInt("idUsuario"));
				uro.setNombre(rs.getString("nombres"));
				uro.setApellido(rs.getString("apellidos"));
				uro.setUsuario(rs.getString("usuario"));
				uro.setRol(rs.getString("rol"));
				
				
				permisosUsuarios.add(uro);
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("DATOS: ERROR AL OBTENER DATOS DEL USUARIO");
			e.printStackTrace();
		}
		return permisosUsuarios;
	}
}
