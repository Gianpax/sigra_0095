package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.EstadoActividad;;

public class DTEstados 
{
	PoolConexion pc = PoolConexion.getInstance();
	Connection cn = PoolConexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<EstadoActividad> listarEstado()
	{
		ArrayList<EstadoActividad> estados = new ArrayList<EstadoActividad>();
		String sql = "SELECT * FROM public.\"estadoActividad\"";
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				EstadoActividad ea = new EstadoActividad();
				ea.setIdEstadoAct(rs.getInt("idEstadoActividad"));
				ea.setEstadoAct(rs.getString("nombreEstadoActividad"));
				estados.add(ea);
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("DATOS: ERROR AL OBTENER EL CRONOGRAMA");
			e.printStackTrace();
		}
		return estados;
	}
}
