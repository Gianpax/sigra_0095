package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import entidades.Tutor;

public class DtTutor {
	PoolConexion poolConexion = PoolConexion.getInstance();
	Connection connection = PoolConexion.getConnection();
	ResultSet resultSet = null;
	
	public ArrayList<Tutor> listarTutor()
	{
		ArrayList<Tutor> tut = new ArrayList<Tutor>();
		String sql = "SELECT * from public.\"tblTutor\" ";
		
		try 
		{
			PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			resultSet = ps.executeQuery();
			
			while(resultSet.next())
			{
				Tutor t= new Tutor();
				t.setIdTutor(resultSet.getInt("idTutor"));
				t.setIdUsuario(resultSet.getInt("idUsuario"));
				tut.add(t);
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("DATOS: ERROR AL OBTENER TUTORES");
			e.printStackTrace();
		}
		return tut;
	}
	
	public boolean guardarTutor(int idUsuario)
	{
		boolean guardado = false;
		try 
		{
			this.listarTutor();
			resultSet.moveToInsertRow();
			resultSet.updateInt("idUsuario", idUsuario);
			
			resultSet.insertRow();
			resultSet.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR -> Error al guardar Tutor " + e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public String getTutor(int idTutor) {
		
		String sql = "Select * from public.\"tblTutor\" as t\r\n" + 
				"inner join public.\"tblUsuarios\" as u on u.\"idUsuario\" = t.\"idUsuario\"\r\n" + 
				"where \"idTutor\" = ?";
		
		String tutor = "";
		
		try 
		{
			PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			ps.setInt(1, idTutor);
			resultSet = ps.executeQuery();
			
			while(resultSet.next())
			{
				tutor = resultSet.getString("nombres") + " " + resultSet.getString("apellidos");
			}
			return tutor;
		} 
		catch (SQLException e) 
		{
			System.err.println("DATOS: ERROR AL OBTENER TUTORES");
			e.printStackTrace();
			return tutor;
		}
	}
}
