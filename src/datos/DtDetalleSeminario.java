package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.DetalleSeminario;

public class DtDetalleSeminario {
	PoolConexion poolConexion = PoolConexion.getInstance();
	Connection connection = PoolConexion.getConnection();
	ResultSet resultSet = null;
	
	public ArrayList<DetalleSeminario> listarDetalleSeminario(int idSeminario)
	{
		ArrayList<DetalleSeminario> seminario = new ArrayList<DetalleSeminario>();
		String sql;
		if(idSeminario != 0) {
			sql = "SELECT * FROM public.\"tblDetalleSeminario\" where \"idSeminario\" = ?";
		}else {
			sql = "SELECT * FROM public.\"tblDetalleSeminario\" ";
		}
		
		try 
		{
			PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
					ResultSet.CONCUR_UPDATABLE,ResultSet.HOLD_CURSORS_OVER_COMMIT);
			ps.setInt(1, idSeminario);
			resultSet = ps.executeQuery();
			
			while(resultSet.next())
			{
				DetalleSeminario sem= new DetalleSeminario();
				sem.setIdSeminario(resultSet.getInt("idDetalleSeminario"));
				sem.setIdGrupoFCE(resultSet.getInt("idGrupoFCE"));
				sem.setIdSeminario(resultSet.getInt("idSeminario"));
				seminario.add(sem);
			}
		} 
		catch (SQLException e) 
		{
			System.err.println("DATOS: ERROR AL OBTENER DETALLE SEMINARIO");
			e.printStackTrace();
		}
		return seminario;
	}
	
	public boolean guardarDetalleSeminario(DetalleSeminario detSem)
	{
		boolean guardado = false;
		try 
		{
			this.listarDetalleSeminario(0);
			resultSet.moveToInsertRow();
			resultSet.updateInt("idGrupoFCE", detSem.getIdGrupoFCE());
			resultSet.updateInt("idSeminario", detSem.getIdSeminario());
			
			resultSet.insertRow();
			resultSet.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR -> Error al guardar Detalle de seminario " + e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarDetalleSeminario(DetalleSeminario detSem)
	{
		
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = "DELETE FROM public.\"tblDetalleSeminario\" WHERE \"idDetalleSeminario\" = ?";
		
		try 
		{
			ps = connection.prepareStatement(sql);
			ps.setInt(1, detSem.getIdDetalleSeminario());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR -> Error al eliminar Detalle de seminario " + e.getMessage());
			e.printStackTrace();
		}
	
		return eliminado;
	}
	
	public boolean modificarDetalleSeminario(DetalleSeminario detSem)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = "UPDATE public.\"tblDetalleSeminario\" " + 
				"	SET \"idGrupoFCE\" = ?, \"idSeminario\" = ? " + 
				"	WHERE \"idDetalleSeminario\" = ?";
				
		try 
		{
 			ps = connection.prepareStatement(sql);
			ps.setInt(1, detSem.getIdGrupoFCE());
			ps.setInt(2, detSem.getIdSeminario());
			ps.setInt(3, detSem.getIdDetalleSeminario());
			ps.executeUpdate();
			modificado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("DATOS: ERROR-> Error al modificar detalle seminario" +e.getMessage());
			e.printStackTrace();
		}
		return modificado;
	}
}
