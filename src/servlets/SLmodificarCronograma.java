package servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTCronograma;
import entidades.Cronograma;

/**
 * Servlet implementation class SLmodificarCronograma
 */
@WebServlet("/SLmodificarCronograma")
public class SLmodificarCronograma extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLmodificarCronograma() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try
		{
			Cronograma c = new Cronograma();
			DTCronograma dtc = new DTCronograma();
			
			String nombreActividad = "";
			String descripcionActividad = "";
			String fechaActividad1;
			String finActividad1;
			int idFase;
			int idEstado;
			int idCronograma;
			
			idCronograma = Integer.parseInt(request.getParameter("midCronograma"));
			nombreActividad = request.getParameter("mnombreActividad");
			descripcionActividad = request.getParameter("mdescActividad");
			fechaActividad1 = request.getParameter("mfechaInicio");
			java.util.Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(fechaActividad1);
			java.sql.Date fechaActividad = new java.sql.Date(utilDate.getTime());
			
			finActividad1 = request.getParameter("mfechaFin");;
			java.util.Date utilDate2 = new SimpleDateFormat("yyyy-MM-dd").parse(finActividad1);
			java.sql.Date finActividad = new java.sql.Date(utilDate2.getTime());
			
			idFase = Integer.parseInt(request.getParameter("midFase"));
			
			idEstado = Integer.parseInt(request.getParameter("midEstado"));
			
			c.setIdCronograma(idCronograma);
			c.setNombreActividad(nombreActividad);
			c.setDescripcionActiviadad(descripcionActividad);
			c.setInicioActividad(fechaActividad);
			c.setFinActividad(finActividad);
			c.setIdFase(idFase);
			c.setIdEstado(idEstado);
			
			if(dtc.modificarActividad(c))
			{
				response.sendRedirect("actividades.jsp");
			}
			else
			{
				response.sendRedirect("actividades.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			System.err.println("SL: ERROR AL MODIFICAR CRONOGRAMA " + e.getMessage());
			e.printStackTrace();
		}
	}

}
