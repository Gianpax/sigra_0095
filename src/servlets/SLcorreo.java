package servlets;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import extra.envioCorreo;

/**
 * Servlet implementation class SLcorreo
 */
@WebServlet("/SLcorreo")
@MultipartConfig(
		fileSizeThreshold=1024*1024*10, 	// 10 MB 
		maxFileSize=1024*1024*50,      	// 50 MB
		maxRequestSize=1024*1024*100)   	// 100 MB
public class SLcorreo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLcorreo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String destinatario;
      	String asunto = "Inscripci�n en SIGRA";
      	String cuerpo = "";
      	envioCorreo env = new envioCorreo();
      	String errores = "Error al enviar a: ";
      	
      	try
      	{ 	
      		Part filePart = request.getPart("excelCorreos"); // Retrieves <input type="file" name="file">
    	    String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
    	    InputStream fileContent = filePart.getInputStream();
    	    System.out.println("Tengo este archivo: "+fileName);
      		//System.out.println(request.getPart("excelCorreos").toString());
      		File imputWorkbook = new File("C://FCE/temp",fileName);
      		String[] correos = request.getParameterValues("correo");
      		if(correos != null) {
      			for (String correo : correos) {
          			destinatario = "";
          			destinatario = correo;
          			
          			cuerpo = ("Ingrese al siguiente enlace para registrarse:");
          			
          			System.out.println(destinatario + " " + asunto + " " + cuerpo);
                  	
                  	if(!env.enviarCorreo(destinatario, asunto, cuerpo)) {
                  		errores+=destinatario;
                  	}
    			}
      		}else {
      			System.out.println("Error en SLcorreo: campos vac�os");
      		}
      		response.sendRedirect("envio_correo_a_estudiantes.jsp");
      	}
      	catch(Exception e)
      	{
      		e.printStackTrace();
			System.out.println("ERROR EN SLcorreo: "+e.getMessage());
			System.out.println(errores);
      	}
	}	
}
