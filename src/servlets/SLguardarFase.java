package servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTFase;
import entidades.Fase;

/**
 * Servlet implementation class SLguardarFase
 */
@WebServlet("/SLguardarFase")
public class SLguardarFase extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarFase() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{
			Fase f = new Fase();
			DTFase dtf = new DTFase();
			
			String nombreFase = "";
			String fechaFase1;
			String finFase1;
			
			nombreFase = request.getParameter("nombreFase");
			
			fechaFase1 = request.getParameter("faseInicio");
			java.util.Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(fechaFase1);
			java.sql.Date fechaFase = new java.sql.Date(utilDate.getTime());
			fechaFase1 = fechaFase.toString();
			
			finFase1 = request.getParameter("faseFin");;
			java.util.Date utilDate2 = new SimpleDateFormat("yyyy-MM-dd").parse(finFase1);
			java.sql.Date finFase = new java.sql.Date(utilDate2.getTime());
			finFase1 = finFase.toString();
			
			f.setTitle(nombreFase);
			f.setStart(fechaFase1);
			f.setEnd(finFase1);
			
			
			if(dtf.guardarFase(f))
			{
				response.sendRedirect("fase.jsp");
			}
			else
			{
				response.sendRedirect("fase.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			System.err.println("SL: ERROR AL GUARDAR FASE " + e.getMessage());
			e.printStackTrace();
		}
	}

}
