package servlets;

import java.io.IOException;
import java.time.LocalDate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTUsuario;
import datos.DtTutor;
import entidades.User;

/**
 * Servlet implementation class SlGuardarTutor
 */
@WebServlet("/SlGuardarTutor")
public class SlGuardarTutor extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SlGuardarTutor() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try 
		{
			User u = new User();
			DTUsuario dtu = new DTUsuario();
			DtTutor dtt = new DtTutor();
			
			if(Integer.parseInt(request.getParameter("sexo"))==1) {
				u.setSexo(false);
			}else if(Integer.parseInt(request.getParameter("sexo"))==2) {
				u.setSexo(true);
			}
			
			u.setUsuario(request.getParameter("usuario"));
			u.setPassword(request.getParameter("pwd"));
			u.setNombres(request.getParameter("nombres"));
			u.setApellidos(request.getParameter("apellidos"));
			u.setCarnet(request.getParameter("carnet"));
			u.setCorreo(request.getParameter("correo"));
			u.setFechaNac(LocalDate.parse(request.getParameter("fechaNac")));
			u.setIdRol(Integer.parseInt(request.getParameter("idRol")));
			
			if(dtu.guardarUsuario(u))
			{
				if(u.getIdRol()==3) {
					dtt.guardarTutor(dtu.getIdUsuario(u));
					response.sendRedirect("registroTutores.jsp");
				}else {
					response.sendRedirect("registroTutores.jsp?error");
				}
			}
			else
			{
				response.sendRedirect("registroTutores.jsp?error");
			}

		} 
		catch (Exception e) 
		{
			System.err.println("SL: ERROR AL GUARDAR Tutor " + e.getMessage());
			e.printStackTrace();
			response.sendRedirect("registroTutores.jsp?error");
		}
	}

}
