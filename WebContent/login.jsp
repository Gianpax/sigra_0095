<%@page import="entidades.Rol"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTRol"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%
    
    //Limpia la CACHE del navegador
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.setDateHeader("Expires", 0);
    response.setDateHeader("Expires", -1);
    %>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>Login - Sigra</title>
  


  <!-- Bootstrap CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="css/elegant-icons-style.css" rel="stylesheet" />
  <link href="css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

<%
		//DESTRUYE LA SESIÓN
		HttpSession hts = request.getSession(false);
		hts.removeAttribute("login");
		hts.invalidate();
%>
<%
	String ruta = "";
    if (request.getParameter("redirect") != null) {
        ruta = request.getParameter("redirect");
    }
%>
</head>

<body>

  <div class="container">
    <form class="login-form" action="./SL_login" method="post">
      <div class="login-wrap">
        <p class="login-img"><i style="color: #023059" class="icon_lock_alt"></i></p>
        <div class="input-group">
          <span class="input-group-addon"><i class="icon_profile"></i></span>
          <input type="text" class="form-control" placeholder="Usuario" autofocus name="username">
        </div>
        <div class="input-group">
          <span class="input-group-addon"><i class="icon_key_alt"></i></span>
          <input type="password" class="form-control" placeholder="Contraseña" name="password">
        </div>
        <div class="form-group">
          <select type="select" class="form-control" placeholder="Rol" name="rol">
          	<%
           		DTRol dtr = new DTRol();
           		ArrayList<Rol> roles = new ArrayList<Rol>();
           		roles = dtr.listarRol();
           		for(Rol r: roles)
           		{
           	%>
           	<option value="<%=r.getIdRol() %>"><%=r.getRol() %></option>
           	<% 		
           		}
           	%>
          </select>
        </div>
        <input type="hidden" value="<%=ruta%>" name="redirect">
        <button class="btn btn-primary btn-lg btn-block" type="submit">Acceder</button>
      </div>
    </form>
    <div class="text-right">
      <div class="credits">
      <br><br><br><br><br><br>
          Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
    </div>
  </div>


</body>

</html>
