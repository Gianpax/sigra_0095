<%@page import="datos.DTUsuario_vs_Rol_Opciones"%>
<%@page import="vistas.Usuario_vs_Rol_Opciones"%>
<%@page import="java.util.ArrayList"%>
<%
Usuario_vs_Rol_Opciones u = new Usuario_vs_Rol_Opciones();
DTUsuario_vs_Rol_Opciones dtu = new DTUsuario_vs_Rol_Opciones();
String nombre= "", apellido ="", rol = "";

		//VALIDACI�N DE LA EXISTENCIA DE LA SESI�N
		String loginUser="";
		loginUser = (String)session.getAttribute("login");
		loginUser = loginUser==null?"":loginUser;
		//VALIDA QUE LA VARIABLE loginUser NO SEA NULL
		
		Integer idRol = 0;
		idRol = (Integer)session.getAttribute("loginRol");
		idRol = idRol==null?0:idRol;
		//VALIDA QUE LA VARIABLE idRol no sea null
		
		if(loginUser.equals(""))
		{
			response.sendRedirect("login.jsp");
		}
		else
		{
			ArrayList<Usuario_vs_Rol_Opciones> usuarios = new ArrayList<Usuario_vs_Rol_Opciones>();
			System.out.println(loginUser);
			usuarios = dtu.permisosUsuario(loginUser,idRol);
			for(Usuario_vs_Rol_Opciones uro : usuarios)
			{
				nombre = uro.getNombre();
				apellido = uro.getApellido();
				rol = uro.getRol();
				break;
			}
			
			HttpSession hts2 = request.getSession(true);
			hts2.setAttribute("nombre", nombre + ' ' + apellido);
			//RECUPERANDO ROL
			HttpSession hts3 = request.getSession(true);
			hts3.setAttribute("rol", rol);
			
			System.out.println("Usuario: "+nombre + ' ' +apellido +" Rol: " +rol);
			
		}
%>

<!-- container section start -->
<body>
  <!--header start-->
    <header class="header dark-bg">
      <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
      </div>

      <!--logo start-->
      <a href="index.jsp" class="logo"><span class="lite">Sigra</span></a>
      <!--logo end-->

      <div class="top-nav notification-row">
        <!-- notificatoin dropdown start-->
        <ul class="nav pull-right top-menu">
          <!-- user login dropdown start-->
          <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="username">Bienvenido: <%=nombre.concat(' '+ apellido) %></span>
                            <b class="caret"></b>
                        </a>
            <ul class="dropdown-menu extended logout">
              <div class="log-arrow-up"></div>
              <li>
                <a href="login.jsp"><i class="icon_key_alt"></i>Cerrar sesi�n</a>
              </li>
            </ul>
          </li>
          <!-- user login dropdown end -->
        </ul>
        <!-- notificatoin dropdown end-->
      </div>
    </header>
    <!--header end-->
    </body>