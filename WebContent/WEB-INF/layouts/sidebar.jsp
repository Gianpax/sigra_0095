 <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
          <li class="active">
            <a class="" href="index.jsp">
                          <i class="fas fa-home"></i>
                          <span>Inicio</span>
                      </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="fas fa-graduation-cap"></i>
                          <span>Estudiantes</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="registroEstudiantes.jsp">Registrar estudiantes</a></li>
              <li><a class="" href="catalogoEstudiantes.jsp">Cat�logo de estudiantes</a></li>
              <li><a class="" href="envio_correo_a_estudiantes.jsp">Enviar enlace de registro a estudiantes</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="fas fa-university"></i>
                          <span>Tutores</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="registroTutores.jsp">Registrar tutores</a></li>
              <li><a class="" href="catalogoTutores.jsp">Cat�logo de tutores</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="far fa-file-alt"></i>
                          <span>Temas</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="registroTema.jsp">Registrar Tema</a></li>
              <li><a class="" href="catalogoTemas.jsp">Cat�logo de Temas</a></li>
            </ul>
          </li>
          <li class="sub-menu">
          	
          	<a class="" href="gruposSeminario.jsp"><i class="fas fa-users"></i>Grupos de seminario</a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="fas fa-user-cog"></i>
                          <span>Gestion de Roles y Permisos<span class="menu-arrow arrow_carrot-right"></span></span>
                          
                      </a>
            <ul class="sub">
              <li><a class="" href="AsignarRol.jsp">Asignar Rol</a></li>
              <li><a class="" href="ModificarRol.jsp">Modificar Rol</a></li>
              <li><a class="" href="EliminarRol.jsp">Eliminar Rol</a></li>
            </ul>
          </li>
          <li class="sub-menu">
          	<a class="nav-link" href="javascript:;">
	          <i class="far fa-calendar"></i>
	          <span>Cronograma<span class="menu-arrow arrow_carrot-right"></span>
	         </a>
	         <ul class="sub">
	         	<li><a class="nav-link" href="calendar.jsp">
	         	<span>Ver Calendario</span></a></li>
	         	<li><a class="" href="fase.jsp" style="font-size: small;">
	         	Nueva Fase de Calendario</a></li>
	         </ul>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    