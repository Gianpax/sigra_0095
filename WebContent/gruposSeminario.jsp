<%@page import="datos.DtTutor"%>
<%@page import="entidades.SeminarioFCE"%>
<%@page import="datos.DtSeminario"%>
<%@page import="entidades.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTUsuario"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>Grupos de Seminario - Sigra</title>

  <!-- Bootstrap CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="css/elegant-icons-style.css" rel="stylesheet" />
  <link href="css/daterangepicker.css" rel="stylesheet" />
  <link href="css/bootstrap-datepicker.css" rel="stylesheet" />
  <link href="css/bootstrap-colorpicker.css" rel="stylesheet" />
  <link href="fontawesome/css/all.css" rel="stylesheet" />
  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  
  <!-- date picker -->

  <!-- color picker -->

  <!-- Custom styles -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
  <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->

    <!-- =======================================================
      Theme Name: NiceAdmin
      Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
      Author: BootstrapMade
      Author URL: https://bootstrapmade.com
    ======================================================= -->
    <%
		//VALIDACIÓN DE LA EXISTENCIA DE LA SESIÓN
		String loginUser="";
		loginUser = (String)session.getAttribute("login");
		//VALIDA QUE LA VARIABLE loginUser NO SEA NULL
		loginUser = loginUser==null?"":loginUser;
		if(loginUser.equals(""))
		{
			response.sendRedirect("login.jsp?redirect=gruposSeminario.jsp");
		}
	%> 	
</head>

<body>

  <!-- container section start -->
  <section id="container" class="">
    <!--header start-->
    <jsp:include page="WEB-INF/layouts/header.jsp"></jsp:include>
    <!--header end-->

    <!--sidebar start-->
    <jsp:include page="WEB-INF/layouts/sidebar.jsp"></jsp:include>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-file-text-o"></i>Grupos de Seminario</h3>
            <ol class="breadcrumb">
              <li><i class="fas fa-home"></i><a href="index.jsp">Inicio</a></li>
              <li><i class="fas fa-users"></i>Grupos de Seminario</li>
            </ol>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                Grupos de Seminario
              </header>
              <div class="card shadow mb-4">
            <div class="card-body">
           	  <a href="registroTutores.jsp" class="col-lg-12" >Nuevo grupo de Seminario <i class="fas fa-plus-square " ></i></a>
              <div class="table-responsive">
                <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Número de Grupo</th>
                      <th>Tutor</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    <%
                    DtSeminario dtSem = new DtSeminario();
                    ArrayList<SeminarioFCE> listaSeminarios = new ArrayList<SeminarioFCE>();
                    DtTutor dtTut = new DtTutor();
                    int i = 0;
                    
                    listaSeminarios = dtSem.listarSeminarios();
                    for(SeminarioFCE sem: listaSeminarios)
                    {
                    %>
                    <tr>
                    	<td><%=sem.getNumGrupo()%></td>
                    	<td><%=dtTut.getTutor(sem.getIdTutor())%></td>
                    	<td>
                    		<span>
                    			<a href="SlEliminarSeminario?id=<%=sem.getIdSeminario() %>">
                    				<i class="fas fa-trash"></i>
                    			</a>
                    		</span>
                    		<span id="modificar" data-toggle="modal" data-target="#modificarSeminario<%=i%>">
                    			<a href="#">
                    				<i class="fas fa-edit"></i>
                    			</a>
                    		</span>
                    	</td>
                    </tr>
                    
                     <!-- MODAL: Modificar Seminario -->
					 <form action="./SLmodificarRol" method="post">
					  <div class="modal fade" id="modificarSeminario<%=i %>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					    <div class="modal-dialog" role="document">
					      <div class="modal-content">
					        <div class="modal-header">
					          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					            <span aria-hidden="true">×</span>
					          </button>
					          <h4 class="modal-title" id="exampleModalLabel">Modificar Seminario</h4>
					        </div>
					        <div class="modal-body">
					        	<input hidden="true" value="<%=sem.getIdSeminario() %>" id="idSeminario<%=i %>" name="idSeminario">
					        	<span>Número de grupo: </span> <input type="text" class="form-control" id="numGrup<%=i %>"
					  				name="numGrup" value="<%=sem.getNumGrupo()%>" />
					  			<span>Tutor: </span> <input type="text" class="form-control" id="tutor<%=i %>"
					  			name="tutor" value="<%=dtTut.getTutor(sem.getIdTutor())%>"/>
					        </div>
					        <div class="modal-footer">
					         	<button type="reset" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					  			<button type="submit" class="btn btn-primary">Guardar</button>
					        </div>
					      </div>
					    </div>
					  </div>
					  </form>
					  
                    
                    <%
                    	i++;
                    } 
                    %>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
            </section>            
          </div>
        </div>
        <!-- page end-->
      </section>
    </section>
    <!--main content end-->
    <div class="text-right">
      <div class="credits">
          <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
          -->
          Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
    </div>
  </section>
  <!-- container section end -->
  
  <!-- javascripts -->
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- nice scroll -->
  <script src="js/jquery.scrollTo.min.js"></script>
  <script src="js/jquery.nicescroll.js" type="text/javascript"></script>

  <!-- jquery ui -->
  <script src="js/jquery-ui-1.9.2.custom.min.js"></script>

  <!--custom checkbox & radio-->
  <script type="text/javascript" src="js/ga.js"></script>
  <!--custom switch-->
  <script src="js/bootstrap-switch.js"></script>
  <!--custom tagsinput-->
  <script src="js/jquery.tagsinput.js"></script>

  <!-- colorpicker -->

  <!-- bootstrap-wysiwyg -->
  <script src="js/jquery.hotkeys.js"></script>
  <script src="js/bootstrap-wysiwyg.js"></script>
  <script src="js/bootstrap-wysiwyg-custom.js"></script>
  <script src="js/moment.js"></script>
  <script src="js/bootstrap-colorpicker.js"></script>
  <script src="js/daterangepicker.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <!-- ck editor -->
  <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>
  <!-- custom form component script for this page-->
  <script src="js/form-component.js"></script>
  <!-- custome script for all page -->
  <script src="js/scripts.js"></script>
  
  <!-- DataTables plugins -->
  <script src="datatables/jquery.dataTables.min.js"></script>
  <script src="datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/datatables-demo.js"></script>
  


</body>

</html>
