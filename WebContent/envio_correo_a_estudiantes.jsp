<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>Form Component | Creative - Bootstrap 3 Responsive Admin Template</title>

  <!-- Bootstrap CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="css/elegant-icons-style.css" rel="stylesheet" />
  <link href="css/daterangepicker.css" rel="stylesheet" />
  <link href="css/bootstrap-datepicker.css" rel="stylesheet" />
  <link href="css/bootstrap-colorpicker.css" rel="stylesheet" />
  <link href="fontawesome/css/all.css" rel="stylesheet" />
  
  
  <!-- date picker -->

  <!-- color picker -->

  <!-- Custom styles -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
  <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->

    <!-- =======================================================
      Theme Name: NiceAdmin
      Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
      Author: BootstrapMade
      Author URL: https://bootstrapmade.com
    ======================================================= -->
</head>

<body>

  <!-- container section start -->
  <section id="container" class="">
    <!--header start-->
    <jsp:include page="WEB-INF/layouts/header.jsp"></jsp:include>
    <!--header end-->

    <!--sidebar start-->
    <jsp:include page="WEB-INF/layouts/sidebar.jsp"></jsp:include>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-file-text-o"></i> Registro de estudiantes</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                Lista de estudiantes a registrar
              </header>
              <div class="panel-body">
                <form class="form-horizontal " method="post" action="./SLcorreo" enctype="multipart/form-data">
                  <!-- <div class="form-group">
                    <label class="col-sm-2 control-label">Correos de Estudiantes</label>
                    <div class="col-sm-10">
	                    <table class="table table-bordered">
		                  <tbody id="invoiceItem">
		                      <tr>
		                          <td><input class="col-sm-2 itemRow" type="checkbox"></td>
		                          <td><input type="text" name="correo" id="correo_1" class="form-control" required></td>
		                      </tr>
		                  </tbody>						
	                    </table>
	                    <div class="col-sm-10">
		                	<button class="btn btn-danger delete" id="removeRows" type="button" onclick=""><b>- Borrar</b></button>
		                    <button class="btn btn-success" id="addRows" type="button" onclick=""><b>+ Agregar M�s</b></button>
	                  	</div>
                    </div>
                  </div>  -->
                  <div class="form-group">
                  	<label class="col-sm-2 control-label" >Importar desde Excel</label>
                  	<div class="col-lg-10">
                  		<input type="file" name="excelCorreos" id="excelCorreos" onchange="<%//llenarTextArea();%>">
                  	</div>
                  </div>
                  <div>
		          	<Button type="submit" class="btn btn-primary btn-user btn-block">Enviar Correos</Button>
		          </div>
                </form>
              </div>
            </section>
          </div>
        </div>
        <!-- page end-->
      </section>
    </section>
    <!--main content end-->
  </section>
  <!-- container section end -->
  <!-- javascripts -->
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- nice scroll -->
  <script src="js/jquery.scrollTo.min.js"></script>
  <script src="js/jquery.nicescroll.js" type="text/javascript"></script>

  <!-- jquery ui -->
  <script src="js/jquery-ui-1.9.2.custom.min.js"></script>

  <!--custom checkbox & radio-->
  <script type="text/javascript" src="js/ga.js"></script>
  <!--custom switch-->
  <script src="js/bootstrap-switch.js"></script>
  <!--custom tagsinput-->
  <script src="js/jquery.tagsinput.js"></script>

  <!-- colorpicker -->

  <!-- bootstrap-wysiwyg -->
  <script src="js/jquery.hotkeys.js"></script>
  <script src="js/bootstrap-wysiwyg.js"></script>
  <script src="js/bootstrap-wysiwyg-custom.js"></script>
  <script src="js/moment.js"></script>
  <script src="js/bootstrap-colorpicker.js"></script>
  <script src="js/daterangepicker.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <!-- ck editor -->
  <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>
  <!-- custom form component script for this page-->
  <script src="js/form-component.js"></script>
  <!-- custome script for all page -->
  <script src="js/scripts.js"></script>
  
  <script>
  $(document).ready(function(){
    $(document).on('click', '#checkAll', function() {          	
      $(".itemRow").prop("checked", this.checked);
    });	
    $(document).on('click', '.itemRow', function() {  	
      if ($('.itemRow:checked').length == $('.itemRow').length) {
        $('#checkAll').prop('checked', true);
      } else {
        $('#checkAll').prop('checked', false);
      }
    });  
    var count = $(".itemRow").length;
    $(document).on('click', '#addRows', function() { 
      count++;
      var htmlRows = '';
      htmlRows += '<tr>';
      htmlRows += '<td><input class="itemRow" type="checkbox"></td>';
      htmlRows += '<td><input type="text" name="correo" id="correo_'+count+'" class="form-control" required></td>';
      htmlRows += '</tr>';
      $('#invoiceItem').append(htmlRows);
    });
    $(document).on('click', '#removeRows', function(){
      $(".itemRow:checked").each(function() {
        $(this).closest('tr').remove();
      });
      $('#checkAll').prop('checked', false);
    });
  });
  
	
  </script>


</body>

</html>
