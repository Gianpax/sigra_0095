<!DOCTYPE html>
<%@page import="entidades.Fase"%>
<%@page import="datos.DTFase"%>
<%@page import="entidades.User"%>
<%@page import="vistas.Usuario_vs_Rol_Opciones"%>
<%@page import="datos.DTUsuario"%>
<%@page import="entidades.Rol"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTRol"%>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>Modificar Fases Calendario - Sigra</title>

  <!-- Bootstrap CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="css/elegant-icons-style.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link href="css/daterangepicker.css" rel="stylesheet" />
  <link href="css/bootstrap-datepicker.css" rel="stylesheet" />
  <link href="css/bootstrap-colorpicker.css" rel="stylesheet" />
  <link href="fontawesome/css/all.css" rel="stylesheet" />
  <!-- date picker -->

  <!-- color picker -->

  <!-- Custom styles -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
  <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->

    <!-- =======================================================
      Theme Name: NiceAdmin
      Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
      Author: BootstrapMade
      Author URL: https://bootstrapmade.com
    ======================================================= -->
    <%
		//VALIDACI�N DE LA EXISTENCIA DE LA SESI�N
		String loginUser="";
		loginUser = (String)session.getAttribute("login");
		//VALIDA QUE LA VARIABLE loginUser NO SEA NULL
		loginUser = loginUser==null?"":loginUser;
		if(loginUser.equals(""))
		{
			response.sendRedirect("login.jsp?redirect=mFase.jsp");
		}
	%> 	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>

<body>

  <!-- container section start -->
  <section id="container" class="">
    <!--header start-->
    <jsp:include page="WEB-INF/layouts/header.jsp"></jsp:include>
    <!--header end-->

    <!--sidebar start-->
    <jsp:include page="WEB-INF/layouts/sidebar.jsp"></jsp:include>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-file-text-o"></i>Modificar Fases</h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="index.jsp">Inicio</a></li>
              <li><i class="far fa-calendar"></i>Calendario Cronograma</li>
              <li>Modificar fases</li>
            </ol>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                Modificar Fases
              </header>
              <div class="panel-body">
               <form action="./SLmodificarFase" method="post">
		         <div class="form-group">
		         	<span>Fase: </span>
		         	<select class="form-control" id="midFase" name="midFase" title="Seleccione una fase" required="required">
		          	<option value="">Seleccione...</option>
		          		<% 
			          		DTFase dtf = new DTFase();
			    			ArrayList<Fase> fases = new ArrayList<Fase>();
		          			fases = dtf.listarFase();
			    			
		          			for(Fase f: fases)
		          			{
			    		%>
		          		<option value=<%=f.getId() %>> <%=f.getTitle() %> </option>
		          		<%
		          			}
		          		%>	
		         	</select>
		         </div>
		          <div class="form-group col-lg-6">
		          	<span>Fecha Inicio: </span>
		          	<input  class="form-control form-control-user" type="date" title="Ingrese la fecha de inicio de la fase" name="mfaseInicio" id="mfaseInicio" />
		          </div>
		          <div class="form-group col-lg-6">
		          	<span>Fecha Fin: </span>
		          	<input  class="form-control form-control-user" type="date" title="Ingrese la fecha fin de la fase" name="mfaseFin" id="mfaseFin" />
		          </div>		          
		          <br>
		          <button type="submit" class="btn btn-primary" data-dismiss="modal">Guardar</button>
		          
		        </form>  
              </div>
            </section>
          </div>
        <!-- page end-->
      </section>
    </section>
    <!--main content end-->
    <div class="text-right">
      <div class="credits">
          <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
          -->
          Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
    </div>
  </section>
  <!-- container section end -->
  <!-- javascripts -->
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- nice scroll -->
  <script src="js/jquery.scrollTo.min.js"></script>
  <script src="js/jquery.nicescroll.js" type="text/javascript"></script>

  <!-- jquery ui -->
  <script src="js/jquery-ui-1.9.2.custom.min.js"></script>

  <!--custom checkbox & radio-->
  <script type="text/javascript" src="js/ga.js"></script>
  <!--custom switch-->
  <script src="js/bootstrap-switch.js"></script>
  <!--custom tagsinput-->
  <script src="js/jquery.tagsinput.js"></script>

  <!-- colorpicker -->

  <!-- bootstrap-wysiwyg -->
  <script src="js/jquery.hotkeys.js"></script>
  <script src="js/bootstrap-wysiwyg.js"></script>
  <script src="js/bootstrap-wysiwyg-custom.js"></script>
  <script src="js/moment.js"></script>
  <script src="js/bootstrap-colorpicker.js"></script>
  <script src="js/daterangepicker.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <!-- ck editor -->
  <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>
  <!-- custom form component script for this page-->
  <script src="js/form-component.js"></script>
  <!-- custom script for all page -->
  <script src="js/scripts.js"></script>
  
  <script type="text/javascript">
  </script>

</body>

</html>
