<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>Registro Tutores - Sigra</title>

  <!-- Bootstrap CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="css/elegant-icons-style.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link href="css/daterangepicker.css" rel="stylesheet" />
  <link href="css/bootstrap-datepicker.css" rel="stylesheet" />
  <link href="css/bootstrap-colorpicker.css" rel="stylesheet" />
   <link href="fontawesome/css/all.css" rel="stylesheet" />
  <!-- date picker -->

  <!-- color picker -->

  <!-- Custom styles -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
  <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->

    <!-- =======================================================
      Theme Name: NiceAdmin
      Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
      Author: BootstrapMade
      Author URL: https://bootstrapmade.com
    ======================================================= -->
    <%
		//VALIDACI�N DE LA EXISTENCIA DE LA SESI�N
		String loginUser="";
		loginUser = (String)session.getAttribute("login");
		//VALIDA QUE LA VARIABLE loginUser NO SEA NULL
		loginUser = loginUser==null?"":loginUser;
		if(loginUser.equals(""))
		{
			response.sendRedirect("login.jsp");
		}
	%> 	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>

<body>

  <!-- container section start -->
  <section id="container" class="">
    <!--header start-->
    <jsp:include page="WEB-INF/layouts/header.jsp"></jsp:include>
    <!--header end-->

    <!--sidebar start-->
    <jsp:include page="WEB-INF/layouts/sidebar.jsp"></jsp:include>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-file-text-o"></i> Registrar Tutores</h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="index.jsp">Inicio</a></li>
              <li><i class="fas fa-university"></i>Tutores</li>
              <li>Registrar Tutores</li>
            </ol>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                Inscripci�n de tutores
              </header>
              <div class="panel-body">
                <form class="form-horizontal" method="post" action="./SlGuardarTutor">
                  <div class="form-group col-lg-6">
                    <label class="col-sm-2 control-label">Nombres:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" required id="nombres" name="nombres">
                    </div>
                  </div>
                  <div class="form-group col-sm-6">
                    <label class="col-sm-2 control-label">Apellidos:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" required id="apellidos" name="apellidos">
                    </div>
                  </div>
                  <div class="form-group col-sm-6">
                    <label class="col-sm-2 control-label">Carnet:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" required id="carnet" name="carnet">
                    </div>
                  </div>
                  <div class="form-group col-sm-6">
                    <label class="col-sm-2 control-label">Correo:</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" required id="correo" name="correo">
                    </div>
                  </div>
                  <div class="form-group col-sm-5">
                    <label class="col-sm-6 control-label">Fecha de nacimiento:</label>
                    <div class="col-sm-4">
                      <input type="date" class="form-control" required id="fechaNac" name="fechaNac">
                    </div>
                  </div>
                  <div class="form-group col-sm-3">
                  	<label class="col-sm-2 control-label">Sexo:</label>
                    <div class="col-sm-4">
                      <div class="radio form-control" style="border-color: transparent;">
                    	<label>
	                    	<input type="radio" class="radio" required id="sexo1" name="sexo" value=1>Hombre
	                    </label>
	                  </div> 
                    </div>
                    <div class="col-sm-4">
                      <div class="radio form-control" style="border-color: transparent;">
                    	<label>
	                    	<input type="radio" class="radio" required id="sexo2" name="sexo" value=2>Mujer
	                    </label>
	                  </div> 
                    </div>
                  </div> 
                  <div class="form-group col-sm-4">
                    <label class="col-sm-2 control-label">Usuario:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control"required id="usuario" name="usuario">
                    </div>
                  </div>
                  <div class="form-group col-sm-6">
                    <label class="col-sm-2 control-label">Contrase�a:</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control"required id="pwd" name="pwd">
                      <span id="compensate"></span>
                    </div>
                  </div>
                  <div class="form-group col-sm-6">
                    <label class="col-sm-4 control-label">Confirmar contrase�a:</label>
                    <div class="col-sm-8">
                      <input type="password" class="form-control"required id="pwdConfirm" name="pwdConfirm">
                      <span id="message"></span>
                    </div>
                  </div>
                  <input type="hidden" id="idRol" name="idRol" value=3>
                  <div class= "form-group col-sm-10">
	                  <button class="btn btn-primary" type="submit"  id="submit" disabled>Guardar</button>
	                  <button class="btn btn-primary" type="reset" id="reset">Cancelar</button>
                  </div>
                </form>
              </div>
            </section>
          </div>
        <!-- page end-->
      </section>
    </section>
    <!--main content end-->
    <div class="text-right">
      <div class="credits">
          <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
          -->
          Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
    </div>
  </section>
  <!-- container section end -->
  <!-- javascripts -->
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- nice scroll -->
  <script src="js/jquery.scrollTo.min.js"></script>
  <script src="js/jquery.nicescroll.js" type="text/javascript"></script>

  <!-- jquery ui -->
  <script src="js/jquery-ui-1.9.2.custom.min.js"></script>

  <!--custom checkbox & radio-->
  <script type="text/javascript" src="js/ga.js"></script>
  <!--custom switch-->
  <script src="js/bootstrap-switch.js"></script>
  <!--custom tagsinput-->
  <script src="js/jquery.tagsinput.js"></script>

  <!-- colorpicker -->

  <!-- bootstrap-wysiwyg -->
  <script src="js/jquery.hotkeys.js"></script>
  <script src="js/bootstrap-wysiwyg.js"></script>
  <script src="js/bootstrap-wysiwyg-custom.js"></script>
  <script src="js/moment.js"></script>
  <script src="js/bootstrap-colorpicker.js"></script>
  <script src="js/daterangepicker.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <!-- ck editor -->
  <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>
  <!-- custom form component script for this page-->
  <script src="js/form-component.js"></script>
  <!-- custom script for all page -->
  <script src="js/scripts.js"></script>
  
  <script type="text/javascript">
  $('#pwd, #pwdConfirm').on('keyup', function () {
	  if ($('#pwd').val() == $('#pwdConfirm').val()) {
	    $('#message').html('La contrase�a coincide').css('color', 'green');
	    $('#compensate').html('<br>');
	    document.getElementById('submit').removeAttribute('disabled');
	  } else {
	    $('#message').html('La contrase�a no coincide').css('color', 'red');
	    $('#compensate').html('<br>');
	  	document.getElementById('submit').setAttribute("disabled","");
	  }
	  if($('#pwd').val() == ""){
		  $('#message').html('').css('color', 'transparent');
		  $('#compensate').html('');
		  document.getElementById('submit').setAttribute("disabled","");
	  }
	});
  </script>

</body>

</html>
