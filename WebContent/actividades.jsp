<%@page import="entidades.EstadoActividad"%>
<%@page import="datos.DTEstados"%>
<%@page import="entidades.Fase"%>
<%@page import="datos.DTFase"%>
<%@page import="entidades.Cronograma"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTCronograma"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin 2 - Tables</title>

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <jsp:include page="WEB-INF/layouts/sidebar.jsp"></jsp:include>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <jsp:include page="WEB-INF/layouts/header.jsp"></jsp:include>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Actividades</h1>
          <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Lista de Actividades</h6>
              <span id="agregar" data-toggle="modal" data-target="#agregarActividad">
              	<i class="fas fa-plus-square"></i>
              </span>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nombre</th>
                      <th>Descripcion</th>
                      <th>Fecha Inico</th>
                      <th>Fecha Fin</th>
                      <th>Extra</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Nombre</th>
                      <th>Descripcion</th>
                      <th>Fecha Inico</th>
                      <th>Fecha Fin</th>
                      <th>Extra</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <%
                    String idFase = "";
                    int idFase2;
                    idFase = request.getParameter("id");
                    idFase = idFase == null?"0":idFase;
                    idFase2 = Integer.parseInt(idFase);
                    
                    DTCronograma dtc = new DTCronograma();
                    ArrayList<Cronograma> listaActividades = new ArrayList<Cronograma>();
                    
                    listaActividades = dtc.listarCronogramas(idFase2);
                    
                    for(Cronograma a: listaActividades)
                    {
                    %>
                    <tr>
                    	<td><%=a.getNombreActividad() %></td>
                    	<td><%=a.getDescripcionActiviadad() %></td>
                    	<td><%=a.getInicioActividad() %></td>
                    	<td><%=a.getFinActividad() %></td>
                    	<td>
                    		<span>
                    			<a onclick="eliminarActividad(<%=a.getIdCronograma() %>);">
                    				<i class="fas fa-trash"></i>
                    			</a>
                    		</span>
                    		<span id="modificar" data-toggle="modal" data-target="#modificarActividad">
                    			<i class="fas fa-edit"></i>
                    		</span>
                    	</td>
                    </tr>
                    <%
                    } 
                    %>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <jsp:include page="WEB-INF/layouts/footer.jsp"></jsp:include>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  
  <!-- MODAL: Agregar Actividad -->
  <form action="./SLguardarCronograma" method="post">
  	<div class="modal fade" id="agregarActividad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar Actividad</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
  			<div class="modal-body">
  				<span>Nombre Actividad: </span><input class="form-control form-control-user" type="text" title="Ingrese el nombre de la actividad" placeholder="Ingrese el nombre de la actividad" name="nombreActividad" id="nombreActividad"/>
		        <span>Descripción: </span><input class="form-control form-control-user" type="text" title="Ingrese la descripción de la actividad" placeholder="Ingrese la descripción de la actividad" name="descActividad" id="descActividad"/>
		        <span>Fecha Inicio: </span><input  class="form-control form-control-user" type="date" title="Ingrese la fecha de inicio de la actividad" name="fechaInicio" id="fechaInicio" />
		        <span>Fecha Fin: </span><input  class="form-control form-control-user" type="date" title="Ingrese la fecha fin de la actividad" name="fechaFin" id="fechaFin" />
		        <span>Fase: </span>
		        <select class="form-control form-control-user" id="idFase" name="idFase" title="Seleccione una fase">
		         <option value="">Seleccione...</option>
		          <% 
			          DTFase dtf = new DTFase();
			    	  ArrayList<Fase> fases = new ArrayList<Fase>();
		          	  fases = dtf.listarFase();
			    	  for(Fase f: fases)
		          	  {
			      %>
		          	<option value=<%=f.getId() %>> <%=f.getTitle() %> </option>
		          	<%
		          	  }
		          	%>  		
		        </select>
		        <span>Estado: </span>
		        <select class="form-control form-control-user" id="idEstado" name="idEstado" title="Seleccione un estado">
		         <option value="">Seleccione...</option>
		          <% 
			          DTEstados dte = new DTEstados();
			    	  ArrayList<EstadoActividad> estados = new ArrayList<EstadoActividad>();
		          	  estados = dte.listarEstado();
			    	  for(EstadoActividad ea: estados)
		          	  {
			      %>
		          	<option value=<%=ea.getIdEstadoAct() %>> <%=ea.getEstadoAct() %> </option>
		          	<%
		          	  }
		          	%>  		
		        </select>
  			</div>
  			<div class="modal-footer">
  				<button type="reset" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
  				<button type="submit" class="btn btn-primary">Guardar</button>
  			</div>
  		</div>
  	</div>
  </div>
  </form>
  
  <!-- MODAL: Modificar Actividad -->
 <form action="./SLmodificarCronograma" method="post">
  <div class="modal fade" id="modificarActividad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modificar Actividad</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
        	<span>ID: </span><input class="form-control form-control-user" type="text" title="Ingrese el ID de la actividad" placeholder="Ingrese el ID de la actividad" name="midCronograma" id="midCronograma"/>
        	<span>Nombre Actividad: </span><input class="form-control form-control-user" type="text" title="Ingrese el nombre de la actividad" placeholder="Ingrese el nombre de la actividad" name="mnombreActividad" id="mnombreActividad"/>
		    <span>Descripción: </span><input class="form-control form-control-user" type="text" title="Ingrese la descripción de la actividad" placeholder="Ingrese la descripción de la actividad" name="mdescActividad" id="mdescActividad"/>
		    <span>Fecha Inicio: </span><input  class="form-control form-control-user" type="date" title="Ingrese la fecha de inicio de la actividad" name="mfechaInicio" id="mfechaInicio" />
		    <span>Fecha Fin: </span><input  class="form-control form-control-user" type="date" title="Ingrese la fecha fin de la actividad" name="mfechaFin" id="mfechaFin" />
		    <span>Fase: </span>
		    <select class="form-control form-control-user" id="midFase" name="midFase" title="Seleccione una fase">
		     <option value="">Seleccione...</option>
		      <% 
			      DTFase dtf2 = new DTFase();
			   	  ArrayList<Fase> fases2 = new ArrayList<Fase>();
		          fases2 = dtf.listarFase();
			      for(Fase f: fases2)
		          {
			  %>
		      <option value=<%=f.getId() %>> <%=f.getTitle() %> </option>
		      <%
		      	  }
		      %>  		
		    </select>
		    <span>Estado: </span>
		    <select class="form-control form-control-user" id="midEstado" name="midEstado" title="Seleccione un estado">
		     <option value="">Seleccione...</option>
		     <%
		     	DTEstados dte2 = new DTEstados();
			    ArrayList<EstadoActividad> estados2 = new ArrayList<EstadoActividad>();
		        estados2 = dte.listarEstado();
			    for(EstadoActividad ea: estados2)
		        {
			 %>
		     <option value=<%=ea.getIdEstadoAct() %>> <%=ea.getEstadoAct() %> </option>
		     <%
		        }
		     %>  		
		    </select>
        </div>
        <div class="modal-footer">
         	<button type="reset" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
  			<button type="submit" class="btn btn-primary">Guardar</button>
        </div>
      </div>
    </div>
  </div>
  </form>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  
  <script type="text/javascript">
  	function eliminarActividad(id)
  	{
  		window.open("SLeliminarCronograma?id="+id);
  	}
  </script>

</body>

</html>
