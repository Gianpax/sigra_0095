<%@page import="entidades.Fase"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTFase"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding = "UTF-8"%>
	
	<%
	//Limpiar CACHE
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-store");
	response.setDateHeader("Expires", 0);
    response.setDateHeader("Expires", -1);
	%>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>Calendario Cronograma - Sigra</title>

  <!-- Bootstrap CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="css/elegant-icons-style.css" rel="stylesheet" />
  <link href="css/daterangepicker.css" rel="stylesheet" />
  <link href="css/bootstrap-datepicker.css" rel="stylesheet" />
  <link href="css/bootstrap-colorpicker.css" rel="stylesheet" />
  <link href="fontawesome/css/all.css" rel="stylesheet" />
  
  <!-- date picker -->

  <!-- color picker -->

  <!-- Custom styles -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet" />
  
  <link href="https://unpkg.com/@fullcalendar/core@4.3.1/main.min.css" rel="stylesheet">
  <link href="https://unpkg.com/@fullcalendar/daygrid@4.3.0/main.min.css" rel="stylesheet">
  <link href="https://unpkg.com/@fullcalendar/timegrid@4.3.0/main.min.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
  <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
<%
		//VALIDACIÓN DE LA EXISTENCIA DE LA SESIÓN
		String loginUser="";
		loginUser = (String)session.getAttribute("login");
		//VALIDA QUE LA VARIABLE loginUser NO SEA NULL
		loginUser = loginUser==null?"":loginUser;
		if(loginUser.equals(""))
		{
			response.sendRedirect("login.jsp?redirect=calendar.jsp");
		}
%>    
</head>

<body>

  <!-- container section start -->
  <section id="container" class="">
    <!--header start-->
    <jsp:include page="WEB-INF/layouts/header.jsp"></jsp:include>
    <!--header end-->

    <!--sidebar start-->
    <jsp:include page="WEB-INF/layouts/sidebar.jsp"></jsp:include>
    <!--sidebar end-->

    <!--Main Content Start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="far fa-calendar"></i>Calendario Cronograma</h3>
            <ol class="breadcrumb">
              <li><i class="fas fa-home"></i><a href="index.jsp">Inicio</a></li>
              <li><i class="far fa-calendar"></i>Calendario Cronograma</li>
            </ol>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                Calendario Cronograma
              </header>
              <div class="card shadow mb-4">
            <div class="card-body">
           	  <a href="mFase.jsp">Modificar Fases</a>
          
          	  <div id="calendar"></div>
            </div>
          </div>
            </section>            
          </div>
        </div>
        <!-- page end-->
      </section>
    </section>
    <!--main content end-->
    <div class="text-right">
      <div class="credits">
        </div>
    </div>
  </section>
  <!-- container section end -->
  <!-- javascripts -->
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- nice scroll -->
  <script src="js/jquery.scrollTo.min.js"></script>
  <script src="js/jquery.nicescroll.js" type="text/javascript"></script>

  <!-- jquery ui -->
  <script src="js/jquery-ui-1.9.2.custom.min.js"></script>

  <!--custom checkbox & radio-->
  <script type="text/javascript" src="js/ga.js"></script>
  <!--custom switch-->
  <script src="js/bootstrap-switch.js"></script>
  <!--custom tagsinput-->
  <script src="js/jquery.tagsinput.js"></script>

  <!-- colorpicker -->

  <!-- bootstrap-wysiwyg -->
  <script src="js/jquery.hotkeys.js"></script>
  <script src="js/bootstrap-wysiwyg.js"></script>
  <script src="js/bootstrap-wysiwyg-custom.js"></script>
  <script src="js/moment.js"></script>
  <script src="js/bootstrap-colorpicker.js"></script>
  <script src="js/daterangepicker.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <!-- ck editor -->
  <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>
  <!-- custom form component script for this page-->
  <script src="js/form-component.js"></script>
  <!-- custome script for all page -->
  <script src="js/scripts.js"></script>
  
  <script src="https://unpkg.com/@fullcalendar/core@4.3.1/main.min.js"></script>
  <script src="https://unpkg.com/@fullcalendar/daygrid@4.3.0/main.min.js"></script>
  <script src="https://unpkg.com/@fullcalendar/timegrid@4.3.0/main.min.js"></script>

  <script>
 	 document.addEventListener('DOMContentLoaded', function() {
 	 	var calendarEl = document.getElementById('calendar');
 	 	
 	 	var calendar = new FullCalendar.Calendar(calendarEl, {
 	 		plugins: [ 'dayGrid', 'timeGrid' ],
 	 		header: {
 	 			left: 'prev,next,today',
	      		center: 'title',
	     		right: 'month,agendaWeek,agendaDay'
		    },
		    
		    editable: true,
		    eventLimit: true,
		    events: "./SLcalendar",
		    textColor: "white",
		    eventClick: function(info) {
		    	<%
		    	DTFase dtf = new DTFase();
		    	ArrayList<Fase> listaFase = new ArrayList<Fase>();
		    	
		    	listaFase = dtf.listarFase();
		    	for(Fase f: listaFase)
                {
		    	%>
		    	window.open("actividades.jsp?id="+info.event.id);
		    	<%
                }
		    	%>
		    }
 	 	});
 	 	calendar.render();
 	 	
 	 });
  </script>

</body>

</html>
